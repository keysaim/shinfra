#!/bin/bash

# This tool is will call the goget.sh, but also will call go build for each get

if [ $# -lt 1 ]; then
    echo "To download and build a list of packages"
    echo "Usage:"
    echo "  $0 <package1> ... <packageN>"
    exit 1
fi

if [ -L $0 ] ; then
    MY_DIR=$(dirname $(readlink -f $0))
else
    MY_DIR=$(dirname $0)
fi

lib_path="$MY_DIR/../lib/lib.sh"
source $lib_path

goget_path="$MY_DIR/goget.sh"
cmd_exit $goget_path "$@"

for i in "${@}"; do
    cmd_exit go build $i
done
