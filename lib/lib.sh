#!/bin/sh

# Basic shell library
# ---------------------SSH section--------------------------
# For any SSH function, it supports only two auth ways:
# * use 'ssh-copy-id' to make it login without password
# * Set the SSH_PRIV_FILE env to provide the ssh private key file
# By default, the ssh always use the root user, but you can set the SSH_USER to overwrite it


RET_VAL=""
#https_proxy_url="http://proxy.test.com:80/"
dns_servers=("8.8.8.8")

if [ -z $SSH_USER ]; then
    SSH_USER=root
fi

# array operations
function array_has () {
    local e
    for e in "${@:2}"; do [[ "$e" == "$1" ]] && return 0; done
    return 1
}

# string operations
function str_split() {
    local str=$1
    local sp=$2
    IFS="$sp" read -r -a array <<< "$str"
    RET_VAL=( ${array[@]} )
}

function str_split_last() {
    str_split $1 $2
    local segs=( ${RET_VAL[@]} )
    local num=${#segs[@]}
    local eidx=$((num - 1))
    RET_VAL=${segs[$eidx]}
}

function str_split_first() {
    str_split $1 $2
    local segs=( ${RET_VAL[@]} )
    RET_VAL=${segs[0]}
}

function str_startswith() {
    local str="$1"
    local sub="$2"

    if [ "$str" == "${sub}*" ]; then
        return 0
    else
        return 1
    fi
}

function str_endswith() {
    local str=`echo "$1" | rev`
    local sub=`echo "$2" | rev`

    str_startswith "$str" "$sub"
    return $?
}

function replace_line() {
    local pattern="$1"
    local new_line="$2"
    local cfile="$3"

    set -x
    sed -i "/$pattern/c\\$new_line" $cfile
    { set +x;} 2> /dev/null
    code=$?
    if [ $code -ne 0 ]; then
        echo "Replace line failed, args: $@"
        exit $code
    fi
    cat $cfile | grep "$pattern"
    echo ""
    return 0
}

function check_exit() {
    local code=$1
    local err="$2"

    if [ $code -ne 0 ]; then
        echo "$err, exit code: $code"
        exit $code
    fi
}

function check_exit_warn() {
    local code=$1
    local err="$2"

    if [ $code -ne 0 ]; then
        echo "$err, exit code: $code"
    fi
}

#===================config file parse================================
function cfg_file_split() {
    local path=$1
    local sections=($(grep -n '^\[' $path | sed 's/\[//' | sed 's/]//'))
    check_exit $? "Failed to parse config file: $path"

    local size=${#sections[@]}
    local last='$'
    for (( idx=$size-1; idx >= 0; idx-- )); do
        local line=${sections[idx]}
        str_split $line ":"
        local start=${RET_VAL[0]}
        local end=$last
        local name=${RET_VAL[1]}
        local line="$start:$end:$name"
        new_secs[idx]="$line"
        last=$start
    done
    RET_VAL=( ${new_secs[@]} )
    #echo "Config file $path split into sections: ${new_secs[@]}"
}

function cfg_file_find_section() {
    local path=$1
    local sec_name=$2

    cfg_file_split $path
    for line in ${RET_VAL[@]}; do
        str_split $line ":"
        local name=${RET_VAL[2]}
        if [ "$name" == "$sec_name" ]; then
            RET_VAL=$line
            return 0
        fi
    done
    echo "Cannot find section $sec_name in config file $path"
    return 1
}

function cfg_file_replace() {
    local path=$1
    local sec_name=$2
    local field="$3"
    local value="$4"

    cfg_file_find_section $path $sec_name
    if [ $? -ne 0 ]; then
        return 1
    fi
    str_split "$RET_VAL" ":"
    local start=${RET_VAL[0]}
    local end=${RET_VAL[1]}
    local name=${RET_VAL[2]}

    set -x
    sed -i -E "$start,$end{s/^$field=.*/$field=$value/}" $path
    { local code=$?;} 2> /dev/null
    { set +x;} 2> /dev/null
    return $code
}

function cfg_file_get() {
    local path=$1
    local sec_name=$2
    local field="$3"

    cfg_file_find_section $path $sec_name
    if [ $? -ne 0 ]; then
        RET_VAL=""
        return 1
    fi
    str_split "$RET_VAL" ":"
    local start=${RET_VAL[0]}
    local end=${RET_VAL[1]}
    local name=${RET_VAL[2]}

    local line=$(sed -n $start,${end}p $path | grep -E "^$field=")
    if [ $? -ne 0 ]; then
        RET_VAL=""
        return 1
    fi
    if [ "$line" == "" ]; then
        echo "No such field: $field in section: $name in the config file: $path"
        RET_VAL=""
        return 1
    fi
    local idx=$(( ${#field} + 2 ))
    local value=$(echo "$line" | cut -c$idx-)

    RET_VAL=$value
    return 0
}

function cfg_file_get_exit() {
    cfg_file_get "$@"
    check_exit $?
}

function check_proxy() {
    env | grep http_proxy
    if [ $? -eq 0 ]; then
        echo "Please remove the proxy before config"
        exit 1
    fi
}

function prepare_dns_servers() {
    local host=$1
    local code=0

    if [ "$host" != "" ]; then
        for ds in "${dns_servers[@]}"; do
            ssh_cmd $host grep "$ds" /etc/resolv.conf
            if [ $? -ne 0 ]; then
                ssh_cmd $host sed -i "'1s/^/nameserver $ds\n/'" /etc/resolv.conf
                code=$?
                if [ $code -ne 0 ]; then
                    return $code
                fi
            fi
        done
    else
        for ds in "${dns_servers[@]}"; do
            cmd_run grep "$ds" /etc/resolv.conf
            if [ $? -ne 0 ]; then
                cmd_run sed -i "1s/^/nameserver $ds\n/" /etc/resolv.conf
                code=$?
                if [ $code -ne 0 ]; then
                    return $code
                fi
            fi
        done
    fi
    return 0
}

function prepare_centos_repos() {
    local host=$1
    local code=0

    local repo_name="$CENTOS7_REPO"
    if [ "$repo_name" == "" ]; then
        repo_name="centos-7-yum-cisco.repo"
    fi
    if [ "$host" != "" ]; then
        if [ -z $SSH_PRIV_FILE ]; then
            cmd_exit_warn scp ../../config/repo/$repo_name $SSH_USER@$host:/etc/yum.repos.d/
        else
            cmd_exit_warn scp -i $SSH_PRIV_FILE ../../config/repo/$repo_name $SSH_USER@$host:/etc/yum.repos.d/
        fi
        code=$?
        ssh_cmd $host mkdir -p /etc/yum.repos.d/bkup/
        ssh_cmd $host mv -f /etc/yum.repos.d/CentOS* /etc/yum.repos.d/bkup/
    else
        cmd_run cp -f ../../config/repo/$repo_name /etc/yum.repos.d/
        code=$?
        cmd_run mkdir -p /etc/yum.repos.d/bkup/
        cmd_run mv -f /etc/yum.repos.d/CentOS* /etc/yum.repos.d/bkup/
    fi
    return $code
}

function k8s_prepare_repos() {
    local host=$1
    local code=0

    local repo_name="$K8S_REPO"
    if [ "$repo_name" == "" ]; then
        repo_name = "k8s.repo"
    fi
    if [ "$host" != "" ]; then
        if [ -z $SSH_PRIV_FILE ]; then
            cmd_exit_warn scp ../../config/repo/$repo_name $SSH_USER@$host:/etc/yum.repos.d/
        else
            cmd_exit_warn scp -i $SSH_PRIV_FILE ../../config/repo/$repo_name $SSH_USER@$host:/etc/yum.repos.d/
        fi
        code=$?
        ssh_cmd $host mkdir -p /etc/yum.repos.d/bkup/
        ssh_cmd $host mv -f /etc/yum.repos.d/CentOS* /etc/yum.repos.d/bkup/
    else
        cmd_run cp -f ../../config/repo/$repo_name /etc/yum.repos.d/
        code=$?
        cmd_run mkdir -p /etc/yum.repos.d/bkup/
        cmd_run mv -f /etc/yum.repos.d/CentOS* /etc/yum.repos.d/bkup/
    fi
    return $code
}

function update_git_repo() {
    local git_path=$1
    
    str_split_last $git_path "/"
    local nstr=$RET_VAL
    str_split_first $nstr "."
    local name=$RET_VAL

    if [ ! -d $name ]; then
        echo "Cloning git repo: $git_path..."
        git clone $git_path
        code=$?
        check_exit_warn $code "Failed to clone git repo: $git_path"
        if [ $? -ne 0 ]; then
            rm -rf $name
        fi
    else
        cd $name
        echo "Pulling git repo: $git_path..."
        git pull
        check_exit_warn $? "Failed to pull git repo: $git_path"
        cd ..
    fi
    RET_VAL=$name
}

# ssh remote command methods
function ssh_cmd() {
    local host=$1
    shift

    if [ -z $SSH_PRIV_FILE ]; then
        set -x
        ssh $SSH_USER@$host "$@"
    else
        set -x
        ssh -i $SSH_PRIV_FILE $SSH_USER@$host "$@"
    fi

    { local code=$?;} 2> /dev/null
    { set +x;} 2> /dev/null
    echo ""
    return $code
}

function ssh_cmd_exit() {
    ssh_cmd "$@"
    local code=$?
    if [ $code -ne 0 ]; then
        echo "cmd failed with code: $code"
        exit $code
    fi
}

function ssh_get() {
    local host="$1"

    if [ -z $SSH_PRIV_FILE ]; then
        cmd_run scp $SSH_USER@$host:$2 $3
    else
        cmd_run scp -i $SSH_PRIV_FILE $SSH_USER@$host:$2 $3
    fi
    return $?
}

function ssh_put() {
    local host="$1"
    shift
    local files=("$@")
    local size=$#
    local len=$(( size - 1 ))
    local last=$(( size - 1 ))

    if [ -z $SSH_PRIV_FILE ]; then
        cmd_run scp "${files[@]:0:$len}" $SSH_USER@$host:${files[last]}
    else
        cmd_run scp -i $SSH_PRIV_FILE "${files[@]:0:$len}" $SSH_USER@$host:${files[last]}
    fi

    return $?
}

function ssh_upload_exit() {
    ssh_put "$@"
    check_exit $?
}

function ssh_replace_line() {
    local host=$1
    local pattern="$2"
    local new_line="$3"
    local cfile="$4"

    ssh_cmd $host sed -i "'/$pattern/c\\$new_line'" $cfile
    code=$?
    if [ $code -ne 0 ]; then
        echo "Replace line failed, args: $@"
        exit $code
    fi
    ssh_cmd $host cat $cfile | grep "$pattern"
    echo ""
    return 0
}

function ssh_cmd_reload_daemon() {
    local host=$1
    local service=$2

    ssh_cmd_exit $host "systemctl daemon-reload"
    ssh_cmd_exit $host "systemctl restart $service"
}

function ssh_service() {
    local host=$1
    local cmd=$2
    shift
    shift
    local svcs=("$@")

    for svc in ${svcs[@]}; do
        ssh_cmd_exit $host systemctl $cmd $svc
    done
}

function ssh_exist_file() {
    ssh_cmd $1 "if [ ! -f '$2' ]; then exit 1; fi"
    return $?
}

function ssh_exist_dir() {
    ssh_cmd $1 "if [ ! -d '$2' ]; then exit 1; fi"
    return $?
}

function ssh_install_with_proxy() {
    local host=$1
    shift

    ssh_cmd_exit $host "https_proxy=$https_proxy_url; yum -y install $@"

    for name in "$@"
    do
        ssh_cmd $host "rpm -qa | grep $name"
        code=$?
        if [ $code -ne 0 ]; then
            echo "The software $name is not installed!"
            exit $code
        fi
    done
}

function cmd_run() {
    set -x
    "$@"
    { local code=$?;} 2> /dev/null
    { set +x;} 2> /dev/null
    return $code
}

function cmd_exit() {
    cmd_run "$@"
    check_exit $? "Failed to run command"
}

function cmd_exit_warn() {
    cmd_run "$@"
    local code=$?
    check_exit_warn $code "Failed to run command"
    return $code
}

# Curl related

function __http_get() {
    local url="$1"
    local dir="$2"
    local host="$3"

    str_endswith "$dir" "/"
    if [ $? -eq 0 ]; then
        dir="$dir/"
    fi
    local name=`echo "$url" | cut -d? -f1 | rev | cut -d/ -f1 | rev`
    local path="$dir$name"
    local cmd="curl -v -o $path '$url'"
    if [ "$host" == "" ]; then
        cmd_run "$cmd"
    else
        ssh_cmd "$host" "$cmd"
    fi
    local code=$?
    if [ $code -ne 0 ]; then
        RET_VAL=""
    else
        RET_VAL=$path
    fi
    return $code
}

function http_get() {
    __http_get "$@"
    return $?
}

function ssh_http_get() {
    __http_get "$2" "$3" "$1"
    return $?
}

#kubernetes methods
function k8s_get_node_ips() {
    local aa=($(kubectl get nodes | grep -v NAME | awk '{print $1}'))
    RET_VAL=(${aa[@]})
}

function k8s_vldt_rc() {
    # Validate the resource file
    local rc=$1
    local name=$2
    local path=$3

    echo "Validate $rc: $name"
    cmd_exit_warn kubectl create --dry-run=true --validate=true -f $path
    local code=$?
    if [ $code -ne 0 ]; then
        echo "The resource file $path is invalid!"
    else
        echo "The resource file $path is valid!"
    fi
    echo ""

    return $code
}

function k8s_vldt_deploy() {
    k8s_vldt_rc "deployment" "$@"
}

function k8s_vldt_service() {
    k8s_vldt_rc "service" "$@"
}

function k8s_crt_rc() {
    local rc=$1
    local name=$2
    local path=$3

    if [ "$K8S_CREATE_IDEMPOTENT" == "true" ]; then
        # Delete if exist
        kubectl get $rc $name
        if [ $? -eq 0 ]; then
            echo "Delete the existing $rc $name"
            k8s_del_rc $rc $name
        fi
    fi
    echo "Create $rc: $name"
    if [ "$rc" == "configmap" ]; then
        cmd_exit kubectl create configmap $name --from-file $path
    else
        cmd_exit kubectl create -f $path
    fi
    kubectl get ${rc}s | head -n 1
    kubectl get ${rc}s | grep "$name "
    if [ $rc == "deployment" ]; then
        kubectl get pods | head -n 1
        kubectl get pods | grep -E "$name-[0-9]{5}"
    fi
    echo ""
}

function k8s_crt_deploy() {
    k8s_crt_rc "deployment" $@
}

function k8s_crt_service() {
    k8s_crt_rc "service" $@
}

function k8s_upd_rc() {
    local rc=$1
    local name=$2
    local path=$3

    echo "Replace $rc: $name"
    cmd_exit kubectl replace -f $path
    kubectl get ${rc}s | head -n 1
    kubectl get ${rc}s | grep "$name "
    if [ $rc == "deployment" ]; then
        kubectl get pods | head -n 1
        kubectl get pods | grep -E "$name-[0-9]{5}"
    fi
    echo ""
}

function k8s_upd_deploy() {
    k8s_upd_rc "deployment" $@
}

function k8s_upd_service() {
    k8s_upd_rc "service" $@
}

function k8s_del_rc() {
    local rc=$1
    local name=$2

    echo "Delete $rc: $name"
    kubectl get ${rc}s | head -n 1
    kubectl get ${rc}s | grep "$name "
    if [ $? -eq 0 ]; then
        cmd_exit kubectl delete $rc $name
    fi
    kubectl get ${rc}s | head -n 1
    kubectl get ${rc}s | grep "$name "
    echo ""
}

function k8s_del_deploy() {
    k8s_del_rc "deployment" $@
}

function k8s_del_service() {
    k8s_del_rc "service" $@
}

function k8s_get_svc_ip() {
    local name=$1

    local ip=$(kubectl get services | grep "$name " | awk '{print $2'})
    RET_VAL=$ip
    if [ "$RET_VAL" == "" ]; then
        return 1
    fi
    return 0
}

function k8s_get_svc_exip() {
    local name=$1

    local ip=$(kubectl get services | grep "$name " | awk '{print $3'})
    if [ "$ip" == "<none>" ]; then
        RET_VAL=""
    else
        RET_VAL=$ip
    fi
    if [ "$RET_VAL" == "" ]; then
        return 1
    fi
    return 0
}

function k8s_get_svc_port() {
    local name=$1

    local port=$(kubectl get services | grep "$name " | awk '{print $4}' | sed -E 's/\/.*//')
    RET_VAL=$port
    if [ "$RET_VAL" == "" ]; then
        return 1
    fi
    return 0
}

function k8s_ctl_svc_help() {
    local name="$1"
    shift
    if [ $# -lt 1 ]; then
        echo "$name <validate/create/update/delete> [deploy/svc/all] [cluster config file]"
        exit 1
    fi
}

function k8s_ctl_svc() {
    local cmd="$1"
    local svc_name="$2"
    local deploy_file="$3"
    local svc_file="$4"
    local op_rc="$5"

    if [ $cmd == "create" ]; then
        if [ "$op_rc" == "" -o "$op_rc" == "all" ]; then
            k8s_crt_deploy $svc_name $deploy_file
            k8s_crt_service $svc_name $svc_file
        elif [ "$op_rc" == "deploy" ]; then
            k8s_crt_deploy $svc_name $deploy_file
        elif [ "$op_rc" == "svc" ]; then
            k8s_crt_service $svc_name $svc_file
        else
            echo "Unsupported create resource: $op_rc"
            exit 1
        fi
    elif [ $cmd == "validate" ]; then
        if [ "$op_rc" == "" -o "$op_rc" == "all" ]; then
            k8s_vldt_deploy $svc_name $deploy_file
            k8s_vldt_service $svc_name $svc_file
        elif [ "$op_rc" == "deploy" ]; then
            k8s_vldt_deploy $svc_name $deploy_file
        elif [ "$op_rc" == "svc" ]; then
            k8s_vldt_service $svc_name $svc_file
        else
            echo "Unsupported validate resource: $op_rc"
            exit 1
        fi
    elif [ $cmd == "replace" -o $cmd == "update" ]; then
        if [ "$op_rc" == "" -o "$op_rc" == "all" ]; then
            k8s_upd_deploy $svc_name $deploy_file
            k8s_upd_service $svc_name $svc_file
        elif [ "$op_rc" == "deploy" ]; then
            k8s_upd_deploy $svc_name $deploy_file
        elif [ "$op_rc" == "svc" ]; then
            k8s_upd_service $svc_name $svc_file
        else
            echo "Unsupported update resource: $op_rc"
            exit 1
        fi
    elif [ $cmd == "delete" ]; then
        if [ "$op_rc" == "" -o "$op_rc" == "all" ]; then
            k8s_del_deploy $svc_name
            k8s_del_service $svc_name
        elif [ "$op_rc" == "deploy" ]; then
            k8s_del_deploy $svc_name
        elif [ "$op_rc" == "svc" ]; then
            k8s_del_service $svc_name
        else
            echo "Unsupported delete resource: $op_rc"
            exit 1
        fi
    fi
}

# omd operations
function omd_update_minion_cfg() {
    local role_ws_dir=$1
    local svc_salt="$2"

    k8s_get_svc_ip "$svc_salt"
    local salt_ip="$RET_VAL"
    cmd_exit sed -i -E "s/^master:.*/master: $salt_ip/" $role_ws_dir/minion

    k8s_get_node_ips
    for ip in ${RET_VAL[@]}
    do
        ssh_cmd_exit $ip rm -rf $role_ws_dir/minion
        if [ -z $SSH_PRIV_FILE ]; then
            cmd_exit scp $role_ws_dir/minion $SSH_USER@$ip:$role_ws_dir/
        else
            cmd_exit scp -i $SSH_PRIV_FILE $role_ws_dir/minion $SSH_USER@$ip:$role_ws_dir/
        fi
    done
}

# system operations
function sys_service() {
    local cmd=$1
    shift
    local svcs=("$@")

    for svc in ${svcs[@]}; do
        cmd_exit systemctl $cmd $svc
    done
}

function sys_install_with_proxy() {
    https_proxy=$https_proxy_url
    cmd_run yum -y install "$@"
    local code=$?
    unset https_proxy
    check_exit $code
}
