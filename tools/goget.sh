#!/bin/bash

# This tool is used when the golang.org is not available. It will try to get the packages from the github
# For example, for package 'golang.org/x/crypto', the github url is 'github.com/golang/crypto'

if [ $# -lt 1 ]; then
    echo "To download a list of packages"
    echo "Usage:"
    echo "  $0 <package1> ... <packageN>"
    exit 1
fi

# Find the lib.sh
if [ -L $0 ] ; then
    MY_DIR=$(dirname $(readlink -f $0))
else
    MY_DIR=$(dirname $0)
fi

lib_path="$MY_DIR/../lib/lib.sh"
source $lib_path

gox_dir="$GOPATH/src/golang.org/x"
mkdir -p $gox_dir

cd $gox_dir
for i in "${@}"; do
    pkg="$i"
    str_startswith $pkg "golang.org"
    if [ $? -ne 0 ]; then
        str_split "$pkg" "/"
        name="${RET_VAL[2]}"
        if [ ! -d $name ]; then
            git_repo="https://github.com/golang/$name.git"
            cmd_exit git clone $git_repo
        else
            echo "The repo '$gox_dir/$name' already exists, skip get for package '$pkg'"
        fi
    else
        cmd go get -v "$pkg"
    fi

done
cd - > /dev/null
